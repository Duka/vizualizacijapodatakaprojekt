import pandas as pd

df_happiness = pd.read_csv('happiness.csv')
df_corruption = pd.read_csv('corruption.csv')

columns_to_drop_happiness = ['GDP per capita', 'Social support', 'Healthy life expectancy', 'Freedom to make life choices', 'Generosity']
df_happiness = df_happiness.drop(columns=columns_to_drop_happiness)

columns_to_drop_corruption = ['annual_income']
df_corruption = df_corruption.drop(columns=columns_to_drop_corruption)

countries_happiness = df_happiness['Country or region']
countries_corruption = df_corruption['country']

countries_only_in_happiness = set(countries_happiness) - set(countries_corruption)
countries_only_in_corruption = set(countries_corruption) - set(countries_happiness)

if countries_only_in_happiness or countries_only_in_corruption:
    print("Countries only in happiness.csv:")
    print(countries_only_in_happiness)

    print("\nCountries only in corruption.csv:")
    print(countries_only_in_corruption)

for country in countries_only_in_corruption:
    corruption_value = df_corruption[df_corruption['country'] == country]['corruption_index'].values[0]
    new_row = {
        'Overall rank': None,
        'Country or region': country,
        'Score of the average': corruption_value * 100,
        'Perceptions of corruption': corruption_value * 100
    }
    df_happiness = pd.concat([df_happiness, pd.DataFrame([new_row])], ignore_index=True)

for country in countries_only_in_happiness:
    perception_of_corruption = df_happiness[df_happiness['Country or region'] == country]['Perceptions of corruption'].values[0]
    new_row = {
        'country': country,
        'corruption_index': perception_of_corruption * 100
    }
    df_corruption = pd.concat([df_corruption, pd.DataFrame([new_row])], ignore_index=True)

df_happiness['Overall rank'] = df_happiness['Overall rank'].fillna(250)
df_happiness['Score of the average'] = df_happiness['Score of the average'].fillna(df_happiness['Score of the average'].mean())

df_happiness.to_json('happiness_filled.json', orient='records', lines=True)
df_corruption.to_json('corruption_filled.json', orient='records', lines=True)

print("Filled dataframes have been saved to happiness_filled.json and corruption_filled.json")
